\documentclass[answers]{exam}
\usepackage[margin=1in]{geometry}
\input{include.d/macros}

\title{Mathematics for Programming}
\author{ Paul Modlin }
\date{\today}

\begin{document}

\section{Homework for 1.3}
\begin{questions}
	\question
		What is the \emph{largest} five-bit (unsigned) number?
		Answer in both binary and decimal.

	\begin{solution}
	\begin{multicols}{2}
	\begin{itemize}
		\item Binary: $11111_\bin$
		\item Decimal: $2^5 - 1 = 32 - 1 = 31$
	\end{itemize}
	\end{multicols}
	\end{solution}

	\question
		What is the \emph{smallest} five-bit (unsigned) number?
		Answer in both binary and decimal.

	\begin{solution}
	\begin{multicols}{2}
	\begin{itemize}
		\item Binary: $00000_\bin = 0_\bin$
		\item Decimal: $0_\dec$
	\end{itemize}
	\end{multicols}
	\end{solution}

\end{questions}
\end{document}
